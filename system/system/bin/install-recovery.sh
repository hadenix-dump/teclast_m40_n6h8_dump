#!/system/bin/sh
if ! applypatch --check EMMC:/dev/block/platform/soc/soc.ap-apb/71400000.sdio/by-name/recovery:41943040:cd3bec527fa0ae5086c3d101681cbe7f367da6cf; then
  applypatch  \
          --patch /system/recovery-from-boot.p \
          --source EMMC:/dev/block/platform/soc/soc.ap-apb/71400000.sdio/by-name/boot:36700160:158d4a1849c3508547df2135456ce3aa2971b958 \
          --target EMMC:/dev/block/platform/soc/soc.ap-apb/71400000.sdio/by-name/recovery:41943040:cd3bec527fa0ae5086c3d101681cbe7f367da6cf && \
      log -t recovery "Installing new recovery image: succeeded" || \
      log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
